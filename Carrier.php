<?php
/**
 * Created by Rubikin Team.
 * Date: 4/19/14
 * Time: 9:59 PM
 * Question? Come to our website at http://rubikin.com
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Nilead\ShipmentGHNBundle;

use Nilead\ShipmentCommonBundle\Carrier\CarrierAbstract;
use Nilead\ShipmentCommonComponent\Carrier\CarrierInterface;

class Carrier extends CarrierAbstract implements CarrierInterface
{
    /**
     * {@inheritdoc}
     */
    public function getCode()
    {
        return 'ghn';
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'GHN Web Service';
    }

    /**
     * {@inheritdoc}
     */
    public function supportCod()
    {
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function getCurrency()
    {
        return 'VND';
    }

    /**
     * {@inheritdoc}
     */
    public static function getRatesAvailable()
    {
        return [
            1 => 'Gói tiết kiệm',
            2 => 'Gói giao nhanh',
            4 => 'Gói giao qua ngày',
            12 => 'Gói siêu tốc liên tỉnh',
            13 => 'Gói giao nhanh liên tỉnh',
            14 => 'Gói giao tiết kiệm liên tỉnh',
            17 => 'Gói siêu tốc (kiện)',
            18 => 'Gói tiết kiệm (kiện)'
        ];
    }

    public function getLogo()
    {
        return 'images/shipment/carriers/ghn.png';
    }

    public function getSettingFormType()
    {
        return 'nilead_shipment_ghn_setting';
    }

    public function getConfigurationFormType()
    {
        return 'nilead_shipment_ghn_configuration';
    }

    public function getConfigurationFormTemplate()
    {
        return 'NileadShipmentGHNBundle::GHN/ghn_configuration.html.twig';
    }

    public function getDescriptionTemplate()
    {
        return 'NileadShipmentGHNBundle::GHN/ghn_description.html.twig';
    }
}
