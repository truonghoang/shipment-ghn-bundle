<?php
/**
 * Created by Rubikin Team.
 * Date: 4/20/14
 * Time: 2:00 AM
 * Question? Come to our website at http://rubikin.com
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Nilead\ShipmentGHNBundle\tests\unit\Message;

use Nilead\ShipmentCommonBundle\tests\unit\TestCase;
use Nilead\ShipmentGHNBundle\Message\SigninResponse;

class SigninResponseTest extends TestCase
{
    protected $request;

    public function signinSuccess()
    {
        $httpResponse = $this->getMockHttpResponse('SigninSuccess.txt');

        $request = $this->getMockRequest();
        $request->shouldReceive('getTestMode')->once()->andReturn(true);
        $response = new SigninResponse($request, $httpResponse->json());

        $this->assertTrue($response->isSuccessful());
        $this->assertEquals('634d3060-fb68-4cf3-91fb-89d6cad102be', $response->getSessionToken());
    }

    public function testFailure()
    {
        $httpResponse = $this->getMockHttpResponse('SigninFailure.txt');

        $request = $this->getMockRequest();
        $request->shouldReceive('getTestMode')->once()->andReturn(true);
        $response = new SigninResponse($request, $httpResponse->json());

        $this->assertFalse($response->isSuccessful());
    }
}
