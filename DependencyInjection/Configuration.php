<?php
/**
 * Created by Rubikin Team.
 * ========================
 * Date: 2014-10-08
 * Time: 11:25:50 PM
 *
 * This file is a part of Nilead project.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Nilead\ShipmentGHNBundle\DependencyInjection;

use Nilead\ResourceBundle\DependencyInjection\AbstractConfiguration;

/**
 * This is the class that validates and merges configuration from your app/config files
 */
class Configuration extends AbstractConfiguration
{
    /**
     * {@inheritDoc}
     */
    protected $rootName = 'nilead_shipment_ghn';
}
