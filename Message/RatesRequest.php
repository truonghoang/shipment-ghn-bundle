<?php
/**
 * Created by Rubikin Team.
 * Date: 4/19/14
 * Time: 11:31 PM
 * Question? Come to our website at http://rubikin.com
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Nilead\ShipmentGHNBundle\Message;


use Nilead\ShipmentCommonBundle\Message\RatesRequestTrait;
use Nilead\ShipmentCommonComponent\Message\RatesRequestInterface;
use Nilead\ShipmentCommonComponent\Model\ShippablePackageInterface;

class RatesRequest extends RequestAbstract implements RatesRequestInterface
{
    use RatesRequestTrait {
        initialize as _initialize;
    }

    public function initialize(ShippablePackageInterface $package, array $parameters = array())
    {
        $parameters = array_merge(
            [
                'FromDistrictCode' => '0201',
                'ToDistrictCode' => '0209'
            ],
            $parameters
        );

        return $this->_initialize($package, $parameters);
    }

    public function setWeight($weight)
    {
        return $this->setParameter('Weight', $weight);
    }

    public function getWeight()
    {
        return $this->getParameter('Weight');
    }

    public function setFromDistrictCode($fromDistrictCode)
    {
        return $this->setParameter('FromDistrictCode', $fromDistrictCode);
    }

    public function getFromDistrictCode()
    {
        return $this->getParameter('FromDistrictCode');
    }

    public function setToDistrictCode($toDistrictCode)
    {
        return $this->setParameter('ToDistrictCode', $toDistrictCode);
    }

    public function getToDistrictCode()
    {
        return $this->getParameter('ToDistrictCode');
    }

    public function setShippingServiceID($shippingServiceID)
    {
        return $this->setParameter('ShippingServiceID', $shippingServiceID);
    }

    public function getShippingServiceID()
    {
        return $this->getParameter('ShippingServiceID');
    }

    /**
     * {@inheritdoc}
     */
    public function getDefaultParameters()
    {
        return array_merge(
            parent::getDefaultParameters(),
            [
                'SessionToken' => '',
                'Weight' => '',
                'FromDistrictCode' => '',
                'ToDistrictCode' => '',
                'ShippingServiceID' => ''
            ]
        );
    }

    /**
     * {@inheritdoc}
     */
    public function getData()
    {
        $items = [];

        foreach ($this->getMethods() as $methodCode => $method) {
            $items[] = [
                'Weight' => $this->getWeight(),
                'FromDistrictCode' => $this->getFromDistrictCode(),
                'ToDistrictCode' => $this->getToDistrictCode(),
                'ShippingServiceID' => $methodCode
            ];
        }
        $data = array_merge(
            $this->getBaseData(),
            [
                "VersionNo" => null,
                'SessionToken' => $this->getSessionToken(),
                'Items' => $items
            ]
        );

        return $data;
    }

    /**
     * {@inheritdoc}
     */
    public function sendData($data)
    {
        $httpResponse = $this->httpClient->post($this->getEndpoint() . '/CalculateServiceFee', ['json' => json_encode($data)])->send();

        return $this->response = new RatesResponse($this, $httpResponse->json());
    }
}
