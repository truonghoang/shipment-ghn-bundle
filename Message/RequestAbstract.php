<?php
/**
 * Created by Rubikin Team.
 * Date: 4/19/14
 * Time: 11:32 PM
 * Question? Come to our website at http://rubikin.com
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Nilead\ShipmentGHNBundle\Message;

use Nilead\ShipmentCommonBundle\Message\RequestAbstract as BaseRequestAbstract;
use Guzzle\Http\ClientInterface;

abstract class RequestAbstract extends BaseRequestAbstract
{
    /**
     * @var ClientInterface
     */
    protected $httpClient;

    protected $apiVersion = '9999';

    protected $liveEndpoint = 'http://103.20.148.184:8064/Restful/GHNService.svc';
    protected $developerEndpoint = 'http://103.20.148.184:8064/Restful/GHNService.svc';

    public function setClientID($clientID){
        return $this->setParameter('ClientID', $clientID);
    }

    public function getClientID()
    {
        return $this->getParameter('ClientID');
    }

    public function setPassword($password){
        return $this->setParameter('Password', $password);
    }

    public function getPassword()
    {
        return $this->getParameter('Password');
    }

    public function setSessionToken($sessionToken){
        return $this->setParameter('SessionToken', $sessionToken);
    }

    public function getSessionToken()
    {
        return $this->getParameter('SessionToken');
    }

    public function getDefaultParameters()
    {
        return [
            'ClientID' => '',
            'Password' => '',
            'SessionToken' => ''
        ];
    }

    protected function getBaseData()
    {
        $data = array(
            'ClientID' => $this->getClientID(),
            'Password' => $this->getPassword()
        );

        return $data;
    }
}
