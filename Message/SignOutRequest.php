<?php
/**
 * Created by Rubikin Team.
 * Date: 5/14/14
 * Time: 9:58 PM
 * Question? Come to our website at http://rubikin.com
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Nilead\ShipmentGHNBundle\Message;


class SignoutRequest extends RequestAbstract
{
    public function getDefaultParameters()
    {
        return array_merge(parent::getDefaultParameters(),
            [
                'SessionToken' => ''
            ]);
    }

    public function getData()
    {
        $data = [];

        $data['SessionToken'] = $this->getSessionToken();

        return $data;
    }

    public function sendData($data)
    {
        $httpResponse = $this->httpClient->post($this->getEndpoint() . '/SignOut', array('Content-Type' => 'application/json'), json_encode($data))->send();

        return $this->response = new SigninResponse($this, $httpResponse->json());
    }
}
