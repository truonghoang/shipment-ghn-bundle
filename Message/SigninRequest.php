<?php
/**
 * Created by Rubikin Team.
 * Date: 5/14/14
 * Time: 9:58 PM
 * Question? Come to our website at http://rubikin.com
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Nilead\ShipmentGHNBundle\Message;


class SigninRequest extends RequestAbstract
{
    public function getData()
    {
        $data = [];

        $data['ClientID'] = $this->getClientID();
        $data['Password'] = $this->getPassword();

        return $data;
    }

    public function sendData($data)
    {
        $httpResponse = $this->httpClient->post($this->getEndpoint() . '/SignIn', array('Content-Type' => 'application/json'), json_encode($data))->send();

        return $this->response = new SigninResponse($this, $httpResponse->json());
    }
}
