<?php
/**
 * Created by Rubikin Team.
 * Date: 4/19/14
 * Time: 11:31 PM
 * Question? Come to our website at http://rubikin.com
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Nilead\ShipmentGHNBundle\Message;

use Nilead\ShipmentCommonComponent\Message\CancelRequestInterface;

class CancelOrderRequest extends RequestAbstract implements CancelRequestInterface
{
    /**
     * {@inheritdoc}
     */
    public function setTransactionReference($transactionReference)
    {
        return $this->setParameter('GHNOrderCode', $transactionReference);
    }

    /**
     * {@inheritdoc}
     */
    public function getTransactionReference()
    {
        return $this->getParameter('GHNOrderCode');
    }

    /**
     * {@inheritdoc}
     */
    public function getDefaultParameters()
    {
        return array_merge(parent::getDefaultParameters(),
            [
                'SessionToken' => '',
                'GHNOrderCode' => '',
            ]);
    }

    /**
     * {@inheritdoc}
     */
    public function getData()
    {
        $data = array_merge($this->getBaseData(),
            [
                "GHNOrderCode" => $this->getTransactionReference(),
                'SessionToken' => $this->getSessionToken()
            ]);

        return $data;
    }

    /**
     * {@inheritdoc}
     */
    public function sendData($data)
    {
        $httpResponse = $this->httpClient->post($this->getEndpoint() . '/CancelOrder', array('Content-Type' => 'application/json'), json_encode($data))->send();

        return $this->response = new RatesResponse($this, $httpResponse->json());
    }

}
