<?php
/**
 * Created by Rubikin Team.
 * Date: 7/9/13
 * Time: 4:18 PM
 * Question? Come to our website at http://rubikin.com
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Nilead\ShipmentGHNBundle\Form\Type;


use Nilead\ResourceBundle\Form\Type\AbstractResourceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class CarrierSettingType extends AbstractResourceType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add(
                'Password', 'text', array(
                'label' => 'nilead.name',
                'required' => true,
                'data' => ''
            ))
            ->add(
                'ClientID', 'text', array(
                'label' => 'nilead.id',
                'required' => true,
                'data' => ''
            ));
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver
            ->setDefaults(array(
                'data_class' => null,
            ));
    }

    public function getName()
    {
        return 'nilead_shipment_ghn_setting';
    }
}
